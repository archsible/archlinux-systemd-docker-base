==========================================
Archlinux Docker image with systemd
==========================================

.. image:: https://img.shields.io/gitlab/pipeline/archsible/archlinux-systemd-docker-base/master
        :target: https://gitlab.com/archsible/archlinux-systemd-docker-base/pipelines


*archlinux* docker image with systemd and ssh enabled

* Free software: MIT license


Features
--------

* docker image with systemd and ssh


Usage
--------------

.. code-block:: console

    $ docker pull image registry.gitlab.com/archsible/archlinux-systemd-docker-base:latest


from a `Dockerfile` :

first generate a ssh key-pair,

.. code-block:: console

    $ ssh-keygen -t rsa -f id_rsa


Then use the following Dockerfile as an example,

.. code-block:: console

    FROM registry.gitlab.com/archsible/archlinux-systemd-docker-base:latest
    COPY id_rsa.pub /root/.ssh/authorized_keys


Credits
-------

* TODO

